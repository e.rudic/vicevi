insert into category values (1,'Chuck Norris');
insert into category values (2, 'Škola');
insert into category values (3, 'Mujo');
insert into joke(content, category_id) values ('Zašto je Chuck Norris najjači?'||chr(10)||'Zato što vježba dva dana dnevno',1);
insert into joke(content, category_id) values ('Pita nastavnica hrvatskog jezika mladog osnovnoškolca:'||chr(10)||'Reci ti meni što su to prilozi?'||chr(10)||'Prilozi su: ketchup, majoneza, luk, salata',2);
insert into joke(content, category_id) values ('Pričaju dvije gimnazijalke:'||chr(10)||'Nema mi roditelja doma ovaj vikend!'||chr(10)||'Bože, pa koja si ti sretnica! Možeš učiti naglas!',2);
insert into joke(content,category_id) values ('Došao Mujo u pizzeriju i naručio pizzu. Konobar ga upita:'||chr(10)||'Želite da vam izrežem pizzu na 6 ili 12 komada?'||chr(10)||'Ma na 6 komada, nema šanse da pojedem 12',3);