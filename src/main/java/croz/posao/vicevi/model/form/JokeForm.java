package croz.posao.vicevi.model.form;

import croz.posao.vicevi.model.Category;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import javax.websocket.server.ServerEndpoint;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter

public class JokeForm {
    @NotNull
    @Size(min=10, max=1024)
    private String content;

    @Positive(message = "Category must be selected")
    private Long categoryId;


}
