package croz.posao.vicevi.model;

import com.sun.org.apache.xpath.internal.operations.Bool;
import croz.posao.vicevi.model.form.JokeForm;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@Table(name="joke")
@NoArgsConstructor
@AllArgsConstructor
public class Joke implements Serializable,Comparable<Joke> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id", nullable = false, updatable = false)
    private Long id;

    @Column(name="content", nullable = false)
    private String content;

    @Column(name="likes", nullable = false, columnDefinition = "int default 0")
    private Long likes=0L;

    @Column(name="dislikes", nullable = false, columnDefinition = "int default 0")
    private Long dislikes=0L;

    @ManyToOne
    @JoinColumn(name="category_id" ,referencedColumnName = "id")
    private Category category;

    public Joke(String content, Category category){
        this.content=content;
        this.category=category;
    }

    public Boolean like(){
        this.likes+=1;
        return true;
    }

    public Boolean dislike(){
        this.dislikes+=1;
        return true;
    }

    @Override
    public int compareTo(Joke o) {
        Long thisScore=this.likes-this.dislikes;
        Long otherScore=o.likes-o.dislikes;
        return thisScore.compareTo(otherScore);
    }
}
