package croz.posao.vicevi.model;

import com.sun.xml.internal.ws.developer.Serialization;
import lombok.Getter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="category")
@Getter
public class Category implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id", nullable=false, updatable = false)
    private Long id;

    @Column(name="name")
    private String name;
}
