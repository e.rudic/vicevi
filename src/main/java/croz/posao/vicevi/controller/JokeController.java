package croz.posao.vicevi.controller;

import croz.posao.vicevi.model.Joke;
import croz.posao.vicevi.model.form.JokeForm;
import croz.posao.vicevi.service.CategoryService;
import croz.posao.vicevi.service.JokeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Controller
public class JokeController {
    private JokeService jokeService;
    private CategoryService categoryService;

    @Autowired
    public JokeController(JokeService jokeService, CategoryService categoryService){
        this.jokeService=jokeService;
        this.categoryService= categoryService;
    }

    @ModelAttribute("allCategories")
    public Map<String, Long> getCategories(){
        HashMap<String,Long> categoryMap;
        categoryMap=categoryService.getCategoriesMap();
        return categoryMap;
    }

    @ModelAttribute("allJokes")
    public List<Joke> getJokes(){
        List<Joke> jokeArrayList;
        jokeArrayList=jokeService.getJokeList();
        jokeArrayList.sort((Joke joke1,Joke joke2) -> (int) -(joke1.getLikes()-joke1.getDislikes()-joke2.getLikes()+joke2.getDislikes()));
        return jokeArrayList;
    }

    @GetMapping("/")
    public String rankedJoke(Model model,
                             @RequestParam("page")Optional<Integer> page){
        int currentPage=page.orElse(1);
        int pageSize=10;

        Page<Joke> jokePage=jokeService.findPaginated(PageRequest.of(currentPage - 1, pageSize));
        model.addAttribute("jokePage",jokePage);

        int totalPages=jokePage.getTotalPages();

        if(totalPages>0){
            List<Integer> pageNumbers = IntStream.rangeClosed(1,totalPages).
                    boxed().collect(Collectors.toList());
            model.addAttribute("pageNumbers",pageNumbers);
        }

        return "jokePage";
    }

    @PostMapping("/new")
    public String newJoke(@ModelAttribute @Valid JokeForm jokeForm, BindingResult bindingResult){
        if(bindingResult.hasErrors()){
            return "jokeForm";
        }
        jokeService.saveNewJoke(jokeForm.getContent(),jokeForm.getCategoryId());
        return "redirect:/";
    }

    @GetMapping("/new")
    public String jokeForm(Model model){
        model.addAttribute("jokeForm", new JokeForm());
        return "jokeForm";
    }


    @PostMapping("/like")
    public String like(@RequestParam("joke_id") Long jokeId, @RequestParam("page_number") Optional<Integer> pageNumber){
        jokeService.like(jokeId);
        String page=pageNumber.isPresent()?"?page="+(pageNumber.get()+1):"";
        return "redirect:/" + page;
    }

    @PostMapping("/dislike")
    public String dislike(@RequestParam("joke_id") Long jokeId,@RequestParam("page_number") Optional<Integer> pageNumber){
        jokeService.dislike(jokeId);
        String page=pageNumber.isPresent()?"?page="+(pageNumber.get()+1):"";
        return "redirect:/" + page;
    }
}
