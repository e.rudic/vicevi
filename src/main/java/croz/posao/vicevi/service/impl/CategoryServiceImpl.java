package croz.posao.vicevi.service.impl;

import croz.posao.vicevi.model.Category;
import croz.posao.vicevi.repository.CategoryRepository;
import croz.posao.vicevi.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {
    @Autowired
    CategoryRepository categoryRepository;

    @Override
    public HashMap<String, Long> getCategoriesMap() {
        HashMap<String,Long> categoryMap= new HashMap<>();
        List<Category> categoryList= categoryRepository.findAll();
        for (Category category : categoryList){
            categoryMap.put(category.getName(),category.getId());
        }
        return categoryMap;

    }
}
