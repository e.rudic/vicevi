package croz.posao.vicevi.service.impl;

import croz.posao.vicevi.exception.CategoryNotFoundException;
import croz.posao.vicevi.exception.JokeNotFoundException;
import croz.posao.vicevi.model.Category;
import croz.posao.vicevi.model.Joke;
import croz.posao.vicevi.repository.CategoryRepository;
import croz.posao.vicevi.repository.JokeRepository;
import croz.posao.vicevi.service.JokeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

@Service
public class JokeServiceImpl implements JokeService {

    public static String NO_CATEGORY_MSG="Cannot find category with id: ";
    public static String NO_JOKE_MSG="Cannot find joke with id: ";

    @Autowired
    private JokeRepository jokeRepository;
    @Autowired
    private CategoryRepository categoryRepository;

    @Override
    public Joke saveNewJoke(String content, Long categoryId) {
        Optional<Category> result=categoryRepository.findById(categoryId);
        if (result.isPresent()) {
            Joke joke = new Joke(content, result.get());
            return jokeRepository.save(joke);
        }
        else {
            throw new CategoryNotFoundException(NO_CATEGORY_MSG + categoryId);
        }
    }

    @Override
    public List<Joke> getJokeList() {
        return jokeRepository.findAll();
    }

    @Override
    public Boolean like(Long id) {
        Optional<Joke> result = jokeRepository.findById(id);
        if(!result.isPresent()){
            throw new JokeNotFoundException(NO_JOKE_MSG);
        }
        Joke joke=result.get();
        joke.like();
        jokeRepository.save(joke);
        return true;
    }

    @Override
    public Boolean dislike(Long id) {
        Optional<Joke> result = jokeRepository.findById(id);
        if(!result.isPresent()){
            throw new JokeNotFoundException(NO_JOKE_MSG);
        }
        Joke joke=result.get();
        joke.dislike();
        jokeRepository.save(joke);
        return true;
    }


    @Override
    public Page<Joke> findPaginated(Pageable pageable) {
        int pageSize = pageable.getPageSize();
        int currentPage = pageable.getPageNumber();
        int startItem = currentPage * pageSize;
        List<Joke> jokes=jokeRepository.findAll();

        jokes.sort((Joke joke1,Joke joke2) -> (int) -(joke1.getLikes()-joke1.getDislikes()-joke2.getLikes()+joke2.getDislikes()));

        List<Joke> jokeList;

        if (jokes.size() < startItem) {
            jokeList = Collections.emptyList();
        } else {
            int toIndex = Math.min(startItem + pageSize, jokes.size());
            jokeList = jokes.subList(startItem, toIndex);
        }

        Page<Joke> jokePage
                = new PageImpl<>(jokeList, PageRequest.of(currentPage, pageSize), jokes.size());

        return jokePage;
    }
}
