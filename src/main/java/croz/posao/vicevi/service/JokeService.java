package croz.posao.vicevi.service;

import croz.posao.vicevi.model.Joke;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface JokeService {
    Joke saveNewJoke(String content, Long id);
    List<Joke> getJokeList();
    Boolean like(Long id);
    Boolean dislike(Long id);
    Page<Joke> findPaginated(Pageable pageable);
}
