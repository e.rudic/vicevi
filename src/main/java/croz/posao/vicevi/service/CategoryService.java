package croz.posao.vicevi.service;

import croz.posao.vicevi.model.Category;
import org.springframework.stereotype.Service;

import java.util.HashMap;

@Service
public interface CategoryService {
    HashMap<String, Long> getCategoriesMap();
}
