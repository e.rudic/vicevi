package croz.posao.vicevi.exception;

public class JokeNotFoundException extends RuntimeException {
    public JokeNotFoundException(String message){
        super(message);
    }
}
