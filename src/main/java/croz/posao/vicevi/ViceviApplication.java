package croz.posao.vicevi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ViceviApplication {

	public static void main(String[] args) {
		SpringApplication.run(ViceviApplication.class, args);
	}

}
